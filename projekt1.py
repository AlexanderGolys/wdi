def tekst_powitania(jezyk):
    try:
        return text[jezyk][0]
    except KeyError:
        print(f"Język {jezyk} jest niedostępny. Podaj inny język\n")
        powitanie()
        quit()


def powitanie():
    global text
    text = {
        "de": ("Hallo!", "niemiecki"),
        "sp": ("Bienvenida!", "hiszpański"),
        "en": ("Hello!", "angielski"),
        "it": ("Benvenuto!", "włoski")
    }
    print("Dostępne języki: ")
    for key in text:
        print(f"{key}: {text[key][1]}")
    print("Wybierz język: ")
    jezyk = input()
    print(tekst_powitania(jezyk))


if __name__ == "__main__":
    powitanie()

